# DSSAT + PDI
DSSAT + PDI is a modification of the [Decision Support System for Agrotechnology Transfer (DSSAT)](https://dssat.net/) software, which is the core of an [Open AI gym](https://gym.openai.com/) crop management Reinforcement Learning environment named [gym-DSSAT](https://gitlab.inria.fr/rgautron/gym_dssat_pdi).

DSSAT + PDI is powered by the [PDI Data Interface (PDI)](https://pdi.julien-bigot.fr/master/), that allows loose coupling between the original Fortran code and Python.

## Installing gym-DSSAT
Please follow the instructions given [here](https://gitlab.inria.fr/rgautron/gym_dssat_pdi#installing-gym-dssat).

## Issues
Compiling DSSAT with `-ffpe-trap=overflow` causes an issue ; apparently, the issue only arises when the source file that calls PDI_Init/PD_Finalize is compiled with that flag.
